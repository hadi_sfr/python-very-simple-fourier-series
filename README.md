Find Fourier Series of

```math
f(t) = t^3 e^{at}, -\pi < t < \pi
```

for $`a \in \{0.1, 0.01\}`$ and $`n \in \{5, 10, 20, 30\}`$.