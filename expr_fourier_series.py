#!/usr/bin/env python3


from sys import stderr

import sympy
import mpmath as mp
from matplotlib import pyplot as plt


def main():
    a_list = [0.1, 0.01]
    n_list = [5, 10, 20, 30]
    percision = 63
    beg = -sympy.pi
    end = sympy.pi
    d = (end - beg) / percision

    t = sympy.symbols("t")
    a = sympy.symbols("a", integer=True)
    expr = t ** 3 * mp.e ** (a * t)

    t_list = []
    for i in range(percision):
        t_list.append(beg + d * i)
    print("expr_fourier_series", file=stderr)
    expr_fourier_series = sympy.fourier_series(expr, (t, beg, end))
    print("/expr_fourier_series", file=stderr)

    for a_val in a_list:
        plt.figure()
        ax = plt.subplot(1, 1, 1)
        ax.set_xticks(range(len(t_list)))
        ax.set_xticklabels(t_list, rotation=60, fontsize=9)
        ax.set_title("%s, a = %.2f" % (expr, a_val))
        print("a: %f" % (a_val), file=stderr)
        ax.plot([expr.evalf(subs={t: t_val, a: a_val}) for t_val in t_list], label="original function")
        for n_val in n_list:
            print("\tn: %d" % (n_val), file=stderr)
            ax.plot([expr_fourier_series.truncate(n_val).evalf(subs={t: t_val, a: a_val}) for t_val in t_list], label="n = %d" % n_val)
        ax.legend()
        plt.savefig("%d.png" % a_list.index(a_val))
        # plt.show()


if __name__ == '__main__':
    main()
